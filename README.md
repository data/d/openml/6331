# OpenML dataset: LoanDefaultPrediction

https://www.openml.org/d/6331

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data from training set of the Kaggle Loan Default Prediction - Imperial College London challenge:
https://www.kaggle.com/c/loan-default-prediction
      
This data corresponds to a set of financial transactions associated with individuals. The data has been standardized, de-trended, and anonymized. You are provided with over two hundred thousand observations and nearly 800 features.  Each observation is independent from the previous. 

For each observation, it was recorded whether a default was triggered. In case of a default, the loss was measured. This quantity lies between 0 and 100. It has been normalised, considering that the notional of each transaction at inception is 100. For example, a loss of 60 means that only 40 is reimbursed. If the loan did not default, the loss was 0. You are asked to predict the losses for each observation in the test set.

Missing feature values have been kept as is, so that the competing teams can really use the maximum data available, implementing a strategy to fill the gaps if desired. Note that some variables may be categorical (e.g. f776 and f777).

The competition sponsor has worked to remove time-dimensionality from the data. However, the observations are still listed in order from old to new.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/6331) of an [OpenML dataset](https://www.openml.org/d/6331). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/6331/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/6331/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/6331/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

